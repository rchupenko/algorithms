def sort(arr):

    def qsort(a, left, right):
        if right <= left:
            return
        pivot = a[(left + right) // 2]
        i = left
        j = right
        while i < j:
            while a[i] < pivot:
                i = i + 1
            while a[j] > pivot:
                j = j - 1
            if i <= j:
                if a[i] > a[j]:
                    a[i], a[j] = a[j], a[i]
                i = i + 1
                j = j - 1
        if left < j:
            qsort(a, left, j)
        if i < right:
            qsort(a, i, right)

    length = len(arr)
    if length > 1:
        qsort(arr, 0, length - 1)
    return arr
