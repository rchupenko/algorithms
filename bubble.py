def sort(a: list) -> list:
    def bubble(a: list) -> list:
        len_a = len(a)
        for j in range(len_a):
            for i in range(len_a - j - 1):
                if a[i] > a[i + 1]:
                    a[i], a[i + 1] = a[i + 1], a[i]
        return a
    return bubble(a)
