import unittest

from msort import *


class Test(unittest.TestCase):

    def test_0(self):
        a = []
        self.assertEqual(tuple(sort(a)), tuple([]))

    def test_1(self):
        a = [1]
        self.assertEqual(tuple(sort(a)), tuple([1]))

    def test_2(self):
        a = [2, 2]
        self.assertEqual(tuple(sort(a)), tuple([2, 2]))

    def test_3(self):
        a = [2, 1]
        self.assertEqual(tuple(sort(a)), tuple([1, 2]))

    def test_4(self):
        a = [7, 3, 2, 4, 8, 5, 6, 1]
        self.assertEqual(tuple(sort(a)), tuple([1, 2, 3, 4, 5, 6, 7, 8]))

    def test_5(self):
        a = ['a', 'd', 'e', 'b', 'c']
        self.assertEqual(tuple(sort(a)), tuple(['a', 'b', 'c', 'd', 'e']))


if __name__ == "__main__":
    unittest.main()
