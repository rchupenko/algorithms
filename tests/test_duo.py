import unittest

from duo import *


class Test(unittest.TestCase):

    def test_0(self):
        self.assertEqual(duo(0, 0, 0), INF_ROOTS)

    def test_1(self):
        self.assertEqual(duo(0, 0, 1), INF_ROOTS)

    def test_2(self):
        self.assertEqual(duo(0, 1, 1), "x = -1.0")

    def test_3(self):
        self.assertEqual(duo(1, 1, 1), NO_ROOTS)

    def test_4(self):
        self.assertEqual(duo(1, 2, -3), "x1 = -3.0, x2 = 1.0")

    def test_5(self):
        try:
            test_result = duo(1, 2, None)
        except:
            test_result = GOT_ERROR
        self.assertEqual(test_result, GOT_ERROR)


if __name__ == "__main__":
    unittest.main()
