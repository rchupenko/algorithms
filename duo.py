from math import sqrt


NO_ROOTS = "Solve has not real roots"
INF_ROOTS = "Solve has infinite roots"
GOT_ERROR = "Got error!"


def duo(a: int, b: int, c: int) -> str:
    """
    Solving equation: ax^2 + bx + c = 0
    :param a: coefficient (int)
    :param b: coefficient (int)
    :param c: coefficient (int)
    :return: answer string
    """
    if a == 0:
        if b != 0:
            x = ((-1)*c) / b
            result = f"x = {x}"
        else:
            result = INF_ROOTS
    else:
        d = b*b - 4*a*c
        if d == 0:
            x = -b / (2*a)
            result = f"x = {x}"
        elif d > 0:
            x1 = ((-1)*b + sqrt(d)) / (2*a)
            x2 = ((-1)*b - sqrt(d)) / (2*a)
            res = sorted([x1, x2])
            result = f"x1 = {res[0]}, x2 = {res[1]}"
        else:
            result = NO_ROOTS
    return result
