def sort(a: list) -> list:

    def merge(a: list, b: list) -> list:
        ab = []
        len_a = len(a)
        len_b = len(b)
        i = j = 0
        while i < len_a and j < len_b:
            if a[i] <= b[j]:
                ab.append(a[i])
                i = i + 1
            else:
                ab.append(b[j])
                j = j + 1
        while i < len_a:
            ab.append(a[i])
            i = i + 1
        while j < len_b:
            ab.append(b[j])
            j = j + 1
        return ab

    def merge_sort(a: list) -> list:
        length = len(a)
        if length <= 1:
            return a
        l = merge_sort(a[:length // 2])
        r = merge_sort(a[length // 2:])
        c = merge(l, r)
        for i in range(len(c)):
            a[i] = c[i]
        return a

    return merge_sort(a)
